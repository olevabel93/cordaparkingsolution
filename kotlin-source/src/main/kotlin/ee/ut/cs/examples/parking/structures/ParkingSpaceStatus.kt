package ee.ut.cs.examples.parking.structures

import net.corda.core.serialization.CordaSerializable

@CordaSerializable
enum class ParkingSpaceStatus {
    FREE,RENTED_OUT
}