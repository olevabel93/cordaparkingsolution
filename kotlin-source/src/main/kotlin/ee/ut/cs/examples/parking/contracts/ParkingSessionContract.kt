package ee.ut.cs.examples.parking.contracts

import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction
import ee.ut.cs.examples.parking.structures.ParkingSession
import net.corda.finance.contracts.asset.Cash
import net.corda.finance.utils.sumCash
import java.security.PublicKey

class ParkingSessionContract : Contract {

    companion object {
        @JvmStatic
        val PARKING_SESSION_CONTRACT_ID = "ee.ut.cs.examples.parking.contracts.ParkingSessionContract"
    }

    interface Commands : CommandData {
        class Start : TypeOnlyCommandData(), Commands
        class Update : TypeOnlyCommandData(), Commands
        class Finish : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction): Unit {
        val command = tx.commands.requireSingleCommand<Commands>()
        val setOfSigners = command.signers.toSet()
        when (command.value) {
            is Commands.Start -> verifyStart(tx, setOfSigners)
            is Commands.Update -> verifyUpdate(tx, setOfSigners)
            is Commands.Finish -> verifyFinish(tx, setOfSigners)

            else -> throw IllegalArgumentException("Unrecognised command.")
        }
    }

    private fun keysFromParticipants(parkingSession: ParkingSession): Set<PublicKey> {
        return parkingSession.participants.map {
            it.owningKey
        }.toSet()
    }

    // This only allows one parking session issuance per transaction.
    private fun verifyStart(tx: LedgerTransaction, signers: Set<PublicKey>) = requireThat {
        "No states should be consumed when starting parkingSession." using (tx.inputStates.isEmpty())
        "Only one parkingSession state should be created when starting a parkingSession." using (tx.outputStates.size == 1)
        val parkingSession = tx.outputsOfType<ParkingSession>().single()
        "A newly started parkingSession must have a positive amount." using (parkingSession.amount.quantity >= 0)
        "The lender and tenant cannot be the same identity." using (parkingSession.tenant != parkingSession.renter)
        "Both lender and tenant together only must sign parkingSession settle transaction." using
                (signers == keysFromParticipants(parkingSession))
    }

    private fun verifyUpdate(tx: LedgerTransaction, signers: Set<PublicKey>) = requireThat {
        "Consume previous parking session state." using (tx.inputStates.size == 1)
        "Only one parking session state should be created when updating." using (tx.outputStates.size == 1)
        val inputParkingSession = tx.inputsOfType<ParkingSession>().single()
        val outputParkingSession = tx.outputsOfType<ParkingSession>().single()
        "An updated parking session must have positive amount." using (outputParkingSession.amount.quantity > 0)
        "An updated parking session must have bigger amount than input one." using (inputParkingSession.amount < outputParkingSession.amount)
        "The tenant and lender cannot be the same identity." using (outputParkingSession.tenant != outputParkingSession.renter)
        "Both lender and tenant together only must sign updating transaction." using
                (signers == keysFromParticipants(inputParkingSession))
    }

    private fun verifyFinish(tx: LedgerTransaction, signers: Set<PublicKey>) = requireThat {
        // Check for the presence of an input parking session state.
        val parkingSessions = tx.inputsOfType<ParkingSession>()
        "There must be one input parking session." using (parkingSessions.size == 1)

        // Check there are output cash states.
        // We don't care about cash inputs, the Cash contract handles those.
        val cash = tx.outputsOfType<Cash.State>()
        "There must be output cash." using (cash.isNotEmpty())

        // Check that the cash is being assigned to renter.
        val inputParkingSession = parkingSessions.single()
        val acceptableCash = cash.filter { it.owner == inputParkingSession.renter }
        "There must be output cash paid to the recipient." using (acceptableCash.isNotEmpty())

        // Sum the cash being sent to renter (we don't care about the issuer).
        val sumAcceptableCash = acceptableCash.sumCash().withoutIssuer()
        val amountOutstanding = inputParkingSession.amount
        "The amount paid has to be the amount outstanding." using (amountOutstanding == sumAcceptableCash)

        // Checks the required parties have signed.
        "Both lender and tenant together only must sign finish transaction." using
                (signers == keysFromParticipants(inputParkingSession))
    }
}