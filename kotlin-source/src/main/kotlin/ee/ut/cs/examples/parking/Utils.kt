package ee.ut.cs.examples.parking

import ee.ut.cs.examples.parking.structures.ParkingSpace
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria

 fun getParkingSpace(parkingSpaceReference: UniqueIdentifier, serviceHub: ServiceHub): ParkingSpace {
    val queryCriteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(parkingSpaceReference))
    val parkingSpaceInputStateAndRef = serviceHub.vaultService.queryBy<ParkingSpace>(queryCriteria).states.single()
    val parkingSpaceState = parkingSpaceInputStateAndRef.state.data
    return parkingSpaceState
}