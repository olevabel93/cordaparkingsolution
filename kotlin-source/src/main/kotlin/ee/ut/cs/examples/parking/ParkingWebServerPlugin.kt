package ee.ut.cs.examples.parking

import net.corda.core.messaging.CordaRPCOps
import net.corda.webserver.services.WebServerPluginRegistry
import java.util.function.Function

class ParkingWebServerPlugin : WebServerPluginRegistry {
    override val webApis: List<Function<CordaRPCOps, out Any>> = listOf(Function(::ParkingApi))
    override val staticServeDirs: Map<String, String> = mapOf(
            "parking" to javaClass.classLoader.getResource("parkingWeb").toExternalForm()
    )
}