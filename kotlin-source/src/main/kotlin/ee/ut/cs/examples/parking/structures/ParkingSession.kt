package ee.ut.cs.examples.parking.structures

import net.corda.core.contracts.*
import net.corda.core.flows.FlowLogicRefFactory
import net.corda.core.identity.Party
import net.corda.core.utilities.toBase58String
import ee.ut.cs.examples.parking.flows.UpdateParkingFlow
import java.time.Instant
import java.util.*

class ParkingSession(val amount: Amount<Currency>,
                     val renter: Party,
                     val tenant: Party,
                     val parkingSpaceReference: UniqueIdentifier,
                     override val linearId: UniqueIdentifier = UniqueIdentifier()
                     ) : SchedulableState, LinearState {

    override val participants get() = listOf(renter, tenant)

    private val nextActivityTime = Instant.now().plusSeconds(10)
    override fun nextScheduledActivity(thisStateRef: StateRef, flowLogicRefFactory: FlowLogicRefFactory): ScheduledActivity? {
        return ScheduledActivity(flowLogicRefFactory.create(UpdateParkingFlow.Initiator::class.java, thisStateRef), nextActivityTime)
    }

}