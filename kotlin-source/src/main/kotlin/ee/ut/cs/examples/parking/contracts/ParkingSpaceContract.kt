package ee.ut.cs.examples.parking.contracts

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.transactions.LedgerTransaction
import net.corda.core.contracts.Requirements.using
import ee.ut.cs.examples.parking.structures.ParkingSpace
import ee.ut.cs.examples.parking.structures.ParkingSpaceStatus
import java.security.PublicKey

class ParkingSpaceContract : Contract {

    companion object {
        @JvmStatic
        val CONTRACT_REF = "ee.ut.cs.examples.parking.contracts.ParkingSpaceContract"
    }

    interface Commands : CommandData {
        class Create : TypeOnlyCommandData(), Commands
        class SetFree : TypeOnlyCommandData(), Commands
        class SetRentedOut: TypeOnlyCommandData(), Commands
    }
    override fun verify(tx: LedgerTransaction) {
        val command = tx.commands.requireSingleCommand<Commands>()
        val setOfSigners = command.signers.toSet()
        when (command.value) {
            is Commands.Create -> verifyCreate(tx, setOfSigners)
            is Commands.SetFree -> verifySetFree(tx, setOfSigners)
            is Commands.SetRentedOut -> verifySetRentedOut(tx, setOfSigners)

            else -> throw IllegalArgumentException("Unrecognised command.")
        }
    }

    private fun verifySetFree(tx: LedgerTransaction, ofSigners: Set<PublicKey>) {
        "One input state should be consumed when changing parking space status" using (tx.inputStates.size == 1)
        "One output state should be created when changing parking space status" using (tx.outputStates.size == 1)
        val inputParkingSpace = tx.inputStates.single() as ParkingSpace
        val outputParkingSpace = tx.outputStates.single() as ParkingSpace
        "Input parking space should be on RENTED_OUT status" using (inputParkingSpace.status == ParkingSpaceStatus.RENTED_OUT)
        "Parking space status should be changed to FREE" using (outputParkingSpace.status == ParkingSpaceStatus.FREE)
        "Changed parking space must be signed by both parties" using (ofSigners == keysFromParticipants(outputParkingSpace))
    }

    private fun keysFromParticipants(parkingSpace: ParkingSpace): Set<PublicKey> {
        return parkingSpace.participants.map {
            it.owningKey
        }.toSet()
    }
    private fun verifySetRentedOut(tx: LedgerTransaction, ofSigners: Set<PublicKey>) {
        "One input state should be consumed when changing parking space status" using (tx.inputStates.size == 1)
        "One output state should be created when changing parking space status" using (tx.outputStates.size == 1)
        val inputParkingSpace = tx.inputStates.single() as ParkingSpace
        val outputParkingSpace = tx.outputStates.single() as ParkingSpace
        "Input parking space should be on FREE status" using (inputParkingSpace.status == ParkingSpaceStatus.FREE)
        "Parking space status should be changed to RENTED_OUT" using (outputParkingSpace.status == ParkingSpaceStatus.RENTED_OUT)
        "Changed parking space must be signed by both parties" using (ofSigners == keysFromParticipants(outputParkingSpace))
    }

    private fun verifyCreate(tx: LedgerTransaction, ofSigners: Set<PublicKey>) {
        "No input states should be consumed when creating parking space." using (tx.inputStates.size == 0)
        "One output state should be created when creating parking space" using (tx.outputStates.size == 1)

        val parkingSpace = tx.outputStates.single() as ParkingSpace

        "A newly created parking space should be available" using (parkingSpace.status == ParkingSpaceStatus.FREE)
        "A newly created parking space must be signed by owner only" using (ofSigners == keysFromParticipants(parkingSpace))
    }

}