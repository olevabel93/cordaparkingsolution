package ee.ut.cs.examples.parking

import net.corda.core.serialization.SerializationWhitelist
import net.corda.core.transactions.TransactionBuilder

class ParkingSerializationPlugin: SerializationWhitelist {
    override val whitelist: List<Class<*>>
        get() = listOf(
                TransactionBuilder::class.java
        )
}