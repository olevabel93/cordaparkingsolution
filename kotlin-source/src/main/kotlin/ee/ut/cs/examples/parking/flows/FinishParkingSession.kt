package ee.ut.cs.examples.parking.flows

import co.paralleluniverse.fibers.Suspendable
import net.corda.confidential.IdentitySyncFlow
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndRef
import net.corda.core.flows.*
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import ee.ut.cs.examples.parking.structures.ParkingSession
import ee.ut.cs.examples.parking.contracts.ParkingSessionContract
import ee.ut.cs.examples.parking.structures.ParkingSpace
import net.corda.core.contracts.UniqueIdentifier
import net.corda.finance.contracts.asset.Cash
import net.corda.finance.contracts.getCashBalance

class FinishParkingSession {
    @InitiatingFlow
    @StartableByRPC
    class Initiator(private val parkingSessionReference: UniqueIdentifier) : ParkingSessionBaseFlow() {

        override val progressTracker: ProgressTracker = tracker()

        companion object {
            object PREPARATION : ProgressTracker.Step("Obtaining IOU from vault.")
            object BUILDING : ProgressTracker.Step("Building and verifying transaction.")
            object SIGNING : ProgressTracker.Step("signing transaction.")
            object COLLECTING : ProgressTracker.Step("Collecting counterparty signature.") {
                override fun childProgressTracker() = CollectSignaturesFlow.tracker()
            }
            object FINALISING : ProgressTracker.Step("Finalising transaction.") {
                override fun childProgressTracker() = FinalityFlow.tracker()
            }

            fun tracker() = ProgressTracker(PREPARATION, BUILDING, SIGNING, COLLECTING, FINALISING)
        }

        @Suspendable
        override fun call(): SignedTransaction {
            // Stage 1. Retrieve obligation specified by linearId from the vault.
            progressTracker.currentStep = PREPARATION
            val parkingSessionToFinish = getParkingSession(parkingSessionReference)
            val inputParkingSession = parkingSessionToFinish.state.data

            // Stage 2. Resolve the lender and tenant identity if the obligation is anonymous.
            val tenantIdentity = resolveIdentity(inputParkingSession.tenant)
            val renterIdentity = resolveIdentity(inputParkingSession.renter)

            // Stage 3. This flow can only be initiated by the current recipient.
            check(tenantIdentity == ourIdentity) {
                throw FlowException("Finish parking session flow must be initiated by the tenant.")
            }

            // Stage 4. Check we have enough cash to settle the requested amount.
            val cashBalance = serviceHub.getCashBalance(inputParkingSession.amount.token)
            check(cashBalance.quantity > 0L) {
                throw FlowException("Tenant has no ${inputParkingSession.amount.token} to pay.")
            }
            check(cashBalance >= inputParkingSession.amount) {
                throw FlowException("Tenant has only $cashBalance but needs ${inputParkingSession.amount} to settle.")
            }

            // Stage 5. Create a finish command.
            val finishCommand = Command(
                    ParkingSessionContract.Commands.Finish(),
                    inputParkingSession.participants.map { it.owningKey })

            // Stage 6. Create a transaction builder. Add the finish command and input parking session.
            progressTracker.currentStep = BUILDING
            val builder = TransactionBuilder(firstNotary)
                    .addInputState(parkingSessionToFinish)
                    .addCommand(finishCommand)

            // Stage 7. Get some cash from the vault and add a spend to our transaction builder.
            // We pay cash to the renters payment key.
            val renterPaymentKey = inputParkingSession.renter
            val (_, cashSigningKeys) = Cash.generateSpend(serviceHub, builder, inputParkingSession.amount, renterPaymentKey)

            // Stage 9. Verify and sign the transaction.
            progressTracker.currentStep = SIGNING
            builder.verify(serviceHub)
            val ptx = serviceHub.signInitialTransaction(builder, cashSigningKeys + inputParkingSession.tenant.owningKey)

            // Stage 10. Get counterparty signature.
            progressTracker.currentStep = COLLECTING
            val session = initiateFlow(renterIdentity)
            subFlow(IdentitySyncFlow.Send(session, ptx.tx))
            val stx = subFlow(CollectSignaturesFlow(
                    ptx,
                    setOf(session),
                    cashSigningKeys + inputParkingSession.tenant.owningKey,
                    COLLECTING.childProgressTracker())
            )

            // Stage 11. Finalize the transaction.
            progressTracker.currentStep = FINALISING
            val ftx = subFlow(FinalityFlow(stx, FINALISING.childProgressTracker()))
            subFlow(ChangeParkingSpaceStatusFree.Initiator(inputParkingSession.parkingSpaceReference))
            return ftx
        }


        fun getParkingSession(parkingSessionReference: UniqueIdentifier): StateAndRef<ParkingSession> {
            val queryCriteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(parkingSessionReference))
            val parkingSpaceInputStateAndRef = serviceHub.vaultService.queryBy<ParkingSession>(queryCriteria).states.single()
            return parkingSpaceInputStateAndRef
        }
    }

    @InitiatedBy(Initiator::class)
    class Responder(private val otherFlow: FlowSession) : FlowLogic<SignedTransaction>() {
        @Suspendable
        override fun call(): SignedTransaction {
            subFlow(IdentitySyncFlow.Receive(otherFlow))
            val stx = subFlow(SignTxFlowNoChecking(otherFlow))
            return waitForLedgerCommit(stx.id)
        }
    }
}
