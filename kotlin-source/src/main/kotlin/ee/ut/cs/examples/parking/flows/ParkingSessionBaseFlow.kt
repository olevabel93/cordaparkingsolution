package ee.ut.cs.examples.parking.flows


import net.corda.core.flows.FlowException
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.FlowSession
import net.corda.core.flows.SignTransactionFlow
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction


/**
 * An abstract FlowLogic class that is subclassed by the parking session flows to
 * provide helper methods and classes.
 */
abstract class ParkingSessionBaseFlow : FlowLogic<SignedTransaction>() {

    val firstNotary get() = serviceHub.networkMapCache.notaryIdentities.firstOrNull()
                ?: throw FlowException("No available notary.")


    fun resolveIdentity(abstractParty: AbstractParty): Party {
        return serviceHub.identityService.requireWellKnownPartyFromAnonymous(abstractParty)
    }
}

internal class SignTxFlowNoChecking(otherFlow: FlowSession) : SignTransactionFlow(otherFlow) {
    override fun checkTransaction(tx: SignedTransaction) {

    }
}