package ee.ut.cs.examples.parking.structures

import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party

data class ParkingSpace(override val linearId: UniqueIdentifier = UniqueIdentifier(),
                        val address: String,
                        val owner: Party,
                        val status: ParkingSpaceStatus = ParkingSpaceStatus.FREE,
                        override val participants: List<AbstractParty> = listOf(owner),
                        val dayRate: Double,
                        val nightRate: Double) : LinearState {

}