package ee.ut.cs.examples.parking

import ee.ut.cs.examples.parking.flows.*
import net.corda.core.contracts.Amount
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.utilities.OpaqueBytes
import net.corda.core.utilities.getOrThrow
import ee.ut.cs.examples.parking.structures.ParkingSession
import ee.ut.cs.examples.parking.structures.ParkingSpace
import ee.ut.cs.examples.parking.structures.ParkingSpaceStatus
import net.corda.core.contracts.UniqueIdentifier
import net.corda.finance.contracts.asset.Cash
import net.corda.finance.contracts.getCashBalances
import net.corda.finance.flows.CashIssueFlow
import java.util.*
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.BAD_REQUEST
import javax.ws.rs.core.Response.Status.CREATED

@Path("parking")
class ParkingApi(val rpcOps: CordaRPCOps) {

    private val myIdentity = rpcOps.nodeInfo().legalIdentities.first()

    @GET
    @Path("me")
    @Produces(MediaType.APPLICATION_JSON)
    fun me() = mapOf("me" to myIdentity)

    @GET
    @Path("peers")
    @Produces(MediaType.APPLICATION_JSON)
    fun peers() = mapOf("peers" to rpcOps.networkMapSnapshot()
            .filter { nodeInfo -> nodeInfo.legalIdentities.first() != myIdentity }
            .map { it.legalIdentities.first().name.organisation })

    @GET
    @Path("owed-per-currency")
    @Produces(MediaType.APPLICATION_JSON)
    fun owedPerCurrency() = rpcOps.vaultQuery(ParkingSession::class.java).states
            .filter { (state) -> state.data.renter != myIdentity }
            .map { (state) -> state.data.amount }
            .groupBy({ amount -> amount.token }, { (quantity) -> quantity })
            .mapValues { it.value.sum() }

    @GET
    @Path("parkings")
    @Produces(MediaType.APPLICATION_JSON)
    fun sessions() = rpcOps.vaultQuery(ParkingSession::class.java).states

    @GET
    @Path("cash")
    @Produces(MediaType.APPLICATION_JSON)
    fun cash() = rpcOps.vaultQuery(Cash.State::class.java).states

    @GET
    @Path("cash-balances")
    @Produces(MediaType.APPLICATION_JSON)
    fun getCashBalances() = rpcOps.getCashBalances()

    @GET
    @Path("self-issue-cash")
    fun selfIssueCash(@QueryParam(value = "amount") amount: Int): Response {

        // 1. Prepare issue request.
        val issueAmount = Amount(amount.toLong() * 100, Currency.getInstance("USD"))
        val notary = rpcOps.notaryIdentities().firstOrNull() ?: throw IllegalStateException("Could not find a notary.")
        val issueRef = OpaqueBytes.of(0)
        val issueRequest = CashIssueFlow.IssueRequest(issueAmount, issueRef, notary)

        // 2. Start flow and wait for response.
        val (status, message) = try {
            val flowHandle = rpcOps.startFlowDynamic(CashIssueFlow::class.java, issueRequest)
            val result = flowHandle.use { it.returnValue.getOrThrow() }
            CREATED to result.stx.tx.outputs.single().data
        } catch (e: Exception) {
            BAD_REQUEST to e.message
        }

        // 3. Return the response.
        return Response.status(status).entity(message).build()
    }

    @GET
    @Path("available-spaces")
    @Produces(MediaType.APPLICATION_JSON)
    fun availableSpaces() = rpcOps.vaultQuery(ParkingSpace::class.java).states
            .filter { (state) -> state.data.status == ParkingSpaceStatus.FREE && state.data.owner != myIdentity }

    @GET
    @Path("spaces")
    @Produces(MediaType.APPLICATION_JSON)
    fun allSpaces() = rpcOps.vaultQuery(ParkingSpace::class.java).states
    @GET
    @Path("create-parkingSpace")
    fun createParkingSpace(@QueryParam(value = "address") address: String, @QueryParam(value = "dailyRate") dailyRate: Double, @QueryParam(value = "nightlyRate") nightlyRate: Double): Response {
        val (status, message) = try {

            val flowHandle = rpcOps.startFlowDynamic(
                    CreateParkingSpace::class.java,
                    address,
                    dailyRate,
                    nightlyRate)
            val result = flowHandle.use { it.returnValue.getOrThrow() }
            CREATED to result.tx.outputs.single().data
        } catch (e: Exception) {
            BAD_REQUEST to e.message
        }

        return Response.status(status).entity(message).build()
    }

    @GET
    @Path("start-parkingSession")
    fun startParking(@QueryParam(value = "party") party: String, @QueryParam(value = "spaceId") spaceId: String): Response {
        // 1. Get party objects for the counterparty.
        val renterIdentity = rpcOps.partiesFromName(party, exactMatch = false).singleOrNull()
                ?: throw IllegalStateException("Couldn't lookup node identity for $party.")

        // 3. Start the parking session flow. We block and wait for the flow to return.
        val (status, message) = try {
            val flowHandle = rpcOps.startFlowDynamic(
                    StartParkingSession.Initiator::class.java,
                    renterIdentity,
                    UniqueIdentifier.fromString(spaceId)
            )

            val result = flowHandle.use { it.returnValue.getOrThrow() }
            
            CREATED to result.tx.outputs.single().data
        } catch (e: Exception) {
            BAD_REQUEST to e.message
        }

        // 4. Return the result.
        return Response.status(status).entity(message).build()
    }



    @GET
    @Path("finish-parkingSession")
    fun finishParking(@QueryParam(value = "sessionId") sessionId: String, @QueryParam(value = "spaceId") spaceId: String): Response {

        val (status, message) = try {
            rpcOps.startFlowDynamic(
                    FinishParkingSession.Initiator::class.java,
                    UniqueIdentifier.fromString(sessionId)

            )
            CREATED to "Session finished."
        } catch (e: Exception) {
            BAD_REQUEST to e.message
        }

        return Response.status(status).entity(message).build()
    }
}