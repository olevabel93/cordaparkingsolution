package ee.ut.cs.examples.parking.flows

import co.paralleluniverse.fibers.Suspendable
import ee.ut.cs.examples.parking.contracts.ParkingSpaceContract
import ee.ut.cs.examples.parking.structures.ParkingSpace
import ee.ut.cs.examples.parking.structures.ParkingSpaceStatus
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder


class ChangeParkingSpaceStatusFree {
    @InitiatingFlow
    @StartableByRPC
    open class Initiator(private val parkingSpaceReference: UniqueIdentifier) : FlowLogic<SignedTransaction>() {
        @Suspendable
        override fun call(): SignedTransaction {
            val notary: Party = serviceHub.networkMapCache.notaryIdentities.first()


            val queryCriteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(parkingSpaceReference))
            val parkingSpaceInputStateAndRef = serviceHub.vaultService.queryBy<ParkingSpace>(queryCriteria).states.single()
            val inputParkingSpaceState = parkingSpaceInputStateAndRef.state.data

            val setStatusCommand = Command(ParkingSpaceContract.Commands.SetFree(), listOf(ourIdentity.owningKey, inputParkingSpaceState.owner.owningKey))
            val outputState = inputParkingSpaceState.copy(status = ParkingSpaceStatus.FREE, participants = listOf(ourIdentity, inputParkingSpaceState.owner))
            val outputStateAndContract = StateAndContract(outputState, ParkingSpaceContract.CONTRACT_REF)

            val utx = TransactionBuilder(notary = notary).withItems(
                    outputStateAndContract,
                    parkingSpaceInputStateAndRef,
                    setStatusCommand
            )
            val ptx = serviceHub.signInitialTransaction(utx)
            val session = initiateFlow(inputParkingSpaceState.owner)
            val stx = subFlow(CollectSignaturesFlow(
                    ptx,
                    setOf(session),
                    listOf(ourIdentity.owningKey)
            ))

            val ftx = subFlow(FinalityFlow(stx))

            subFlow(BroadcastTransaction(ftx))
            return ftx
        }
    }

    @InitiatedBy(Initiator::class)
    class Responder(private val otherFlow: FlowSession) : FlowLogic<SignedTransaction>() {
        @Suspendable
        override fun call(): SignedTransaction {
            val stx = subFlow(SignTxFlowNoChecking(otherFlow))
            return waitForLedgerCommit(stx.id)
        }
    }

}
