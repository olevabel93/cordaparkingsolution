package ee.ut.cs.examples.parking.flows

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.contracts.Amount
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import net.corda.core.utilities.seconds
import ee.ut.cs.examples.parking.structures.ParkingSession
import ee.ut.cs.examples.parking.contracts.ParkingSessionContract
import ee.ut.cs.examples.parking.contracts.ParkingSessionContract.Companion.PARKING_SESSION_CONTRACT_ID
import ee.ut.cs.examples.parking.contracts.ParkingSpaceContract
import ee.ut.cs.examples.parking.getParkingSpace
import ee.ut.cs.examples.parking.structures.ParkingSpace
import ee.ut.cs.examples.parking.structures.ParkingSpaceStatus
import net.corda.core.contracts.StateAndContract
import java.util.*


class StartParkingSession {
    @InitiatingFlow
    @StartableByRPC
    open class Initiator(private val renter: Party, private val parkingSpaceReference: UniqueIdentifier) : FlowLogic<SignedTransaction>() {
        val firstNotary
            get() = serviceHub.networkMapCache.notaryIdentities.firstOrNull()
                    ?: throw FlowException("No available notary.")

        companion object {
            object INITIALISING : Step("Performing initial steps.")
            object BUILDING : Step("Building and verifying transaction.")
            object SIGNING : Step("Signing transaction.")
            object COLLECTING : Step("Collecting counterparty signature.") {
                override fun childProgressTracker() = CollectSignaturesFlow.tracker()
            }

            object FINALISING : Step("Finalising transaction.") {
                override fun childProgressTracker() = FinalityFlow.tracker()
            }

            fun tracker() = ProgressTracker(INITIALISING, BUILDING, SIGNING, COLLECTING, FINALISING)
        }

        override val progressTracker: ProgressTracker = tracker()

        @Suspendable
        override fun call(): SignedTransaction {


            // Step 1. Initialisation.
            progressTracker.currentStep = INITIALISING
            val parkingSession = ParkingSession(Amount.zero(Currency.getInstance("USD")), renter, ourIdentity, parkingSpaceReference)
            val ourSigningKey = parkingSession.tenant.owningKey
            val parkingSessionStartCommand = Command(ParkingSessionContract.Commands.Start(), parkingSession.participants.map { it.owningKey })
            val parkingSessionStateAndContract = StateAndContract(parkingSession, ParkingSessionContract.PARKING_SESSION_CONTRACT_ID)

            logger.debug("Init")

            // Step 2. Building.
            progressTracker.currentStep = BUILDING
            val utx = TransactionBuilder(notary = firstNotary).withItems(
                    parkingSessionStateAndContract,
                    parkingSessionStartCommand
            )
            utx.setTimeWindow(serviceHub.clock.instant(), 5.seconds)
            logger.debug("build")
            // Step 3. Sign the transaction.
            progressTracker.currentStep = SIGNING
            val ptx = serviceHub.signInitialTransaction(utx)
            logger.debug("sign")
            // Step 4. Get the counter-party signature.

            progressTracker.currentStep = COLLECTING
            val renterFlow = initiateFlow(renter)
            val stx = subFlow(CollectSignaturesFlow(
                    ptx,
                    setOf(renterFlow),
                    listOf(ourSigningKey),
                    COLLECTING.childProgressTracker())
            )


            // Step 5. Finalise the transaction.
            progressTracker.currentStep = FINALISING
            val ftx = subFlow(FinalityFlow(stx, FINALISING.childProgressTracker()))
            subFlow(ChangeParkingSpaceStatusRentedOut.Initiator(parkingSpaceReference))
            return ftx
        }

    }


    @InitiatedBy(Initiator::class)
    class Responder(private val otherFlow: FlowSession) : FlowLogic<SignedTransaction>() {
        @Suspendable
        override fun call(): SignedTransaction {
            val stx = subFlow(SignTxFlowNoChecking(otherFlow))
            return waitForLedgerCommit(stx.id)
        }
    }
}

