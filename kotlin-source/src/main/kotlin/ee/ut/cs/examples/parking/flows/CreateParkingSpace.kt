package ee.ut.cs.examples.parking.flows

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.StartableByRPC
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import ee.ut.cs.examples.parking.contracts.ParkingSpaceContract
import ee.ut.cs.examples.parking.structures.ParkingSpace

@StartableByRPC
class CreateParkingSpace(private val address: String, private val dayRate: Double, private val nightRate: Double) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {
        val notary: Party = serviceHub.networkMapCache.notaryIdentities.first()

        val parkingSpace = ParkingSpace(owner = ourIdentity, dayRate = dayRate, nightRate = nightRate, address = address)
        val createCommand = Command(ParkingSpaceContract.Commands.Create(), listOf(ourIdentity.owningKey))
        val outputState = StateAndContract(parkingSpace, ParkingSpaceContract.CONTRACT_REF)

        val utx = TransactionBuilder(notary = notary).withItems(outputState, createCommand)

        val stx = serviceHub.signInitialTransaction(utx)
        val ftx = subFlow(FinalityFlow(stx))

        subFlow(BroadcastTransaction(ftx))

        return ftx
    }


}