package ee.ut.cs.examples.parking.flows

import co.paralleluniverse.fibers.Suspendable
import ee.ut.cs.examples.parking.contracts.ParkingSessionContract
import ee.ut.cs.examples.parking.contracts.ParkingSessionContract.Companion.PARKING_SESSION_CONTRACT_ID
import ee.ut.cs.examples.parking.getParkingSpace
import ee.ut.cs.examples.parking.structures.ParkingSession
import net.corda.confidential.IdentitySyncFlow
import net.corda.core.contracts.Amount
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateRef
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import net.corda.core.utilities.seconds
import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*


class UpdateParkingFlow{
    @InitiatingFlow
    @SchedulableFlow
    class Initiator(private val stateRef: StateRef) : FlowLogic<String>() {


        companion object {
            object GENERATING_TRANSACTION : Step("Generating a parking session transaction.")
            object SIGNING_TRANSACTION : Step("Signing transaction with out private key.")
            object COLLECT_SIGNATURE : Step("Collecting counter-party signature.") {
                override fun childProgressTracker() = CollectSignaturesFlow.tracker()
            }
            object FINALISING_TRANSACTION : Step("Recording transaction.") {
                override fun childProgressTracker() = FinalityFlow.tracker()
            }

            fun tracker() = ProgressTracker(GENERATING_TRANSACTION, SIGNING_TRANSACTION, COLLECT_SIGNATURE, FINALISING_TRANSACTION)
        }

        override val progressTracker = tracker()

        @Suspendable
        override fun call(): String {
            logger.debug("update called")
            progressTracker.currentStep = GENERATING_TRANSACTION
            val input = serviceHub.toStateAndRef<ParkingSession>(stateRef)
            val inputParkingSession = input.state.data
            val inputParkingSpace = getParkingSpace(inputParkingSession.parkingSpaceReference, serviceHub)
            val ourSigningKey = inputParkingSession.tenant.owningKey
            val amount = input.state.data.amount.plus(calculateAmount(getRateBaseOnCurrentTime(inputParkingSpace.dayRate, inputParkingSpace.nightRate)))
            val output = ParkingSession(amount, inputParkingSession.renter, inputParkingSession.tenant, inputParkingSession.parkingSpaceReference)
            val updateCommand = Command(ParkingSessionContract.Commands.Update(), input.state.data.participants.map { it.owningKey })
            val txBuilder = TransactionBuilder(serviceHub.networkMapCache.notaryIdentities.first())
                    .addInputState(input)
                    .addOutputState(output, PARKING_SESSION_CONTRACT_ID)
                    .addCommand(updateCommand)
                    .setTimeWindow(serviceHub.clock.instant(), 5.seconds)
            progressTracker.currentStep = SIGNING_TRANSACTION
            val ptx = serviceHub.signInitialTransaction(txBuilder, ourSigningKey)


            progressTracker.currentStep = COLLECT_SIGNATURE
            val renterFlow = initiateFlow(inputParkingSession.renter)
            subFlow(IdentitySyncFlow.Send(renterFlow, ptx.tx))
            val stx = subFlow(CollectSignaturesFlow(
                    ptx,
                    setOf(renterFlow),
                    listOf(ourSigningKey),
                    COLLECT_SIGNATURE.childProgressTracker())
            )

            progressTracker.currentStep = FINALISING_TRANSACTION
            subFlow(FinalityFlow(stx, FINALISING_TRANSACTION.childProgressTracker()))

            if (amount.quantity + getRateBaseOnCurrentTime(inputParkingSpace.dayRate, inputParkingSpace.nightRate) * 100  > 50 * 100) {
                val test = subFlow(FinishParkingSession.Initiator(output.linearId))

            }
            return "Update!"
        }

        private fun calculateAmount(rate: Double): Amount<Currency> {
            val bigDecimalRate = BigDecimal.valueOf(rate)
            return Amount.fromDecimal(bigDecimalRate, Currency.getInstance("USD"))
        }


        private fun getRateBaseOnCurrentTime(dayRate: Double, nightRate: Double): Double {
            val currentTime = LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault())
            if (currentTime.hour >= 6 && currentTime.hour < 22) {
                return dayRate
            }
            return nightRate
        }




    }
    @InitiatedBy(Initiator::class)
    class Responder(private val otherFlow: FlowSession) : FlowLogic<SignedTransaction>() {
        @Suspendable
        override fun call(): SignedTransaction {
            subFlow(IdentitySyncFlow.Receive(otherFlow))
            val stx = subFlow(SignTxFlowNoChecking(otherFlow))
            return waitForLedgerCommit(stx.id)
        }
    }
}