"use strict";

angular.module('demoAppModule').controller('CreateParkingSpaceModalCtrl', function($rootScope, $http, $uibModalInstance, $uibModal, apiBaseURL) {
    const createParkingSpace = this;

    createParkingSpace.form = {};
    createParkingSpace.formError = false;

    createParkingSpace.create = () => {
        if (invalidFormInput()) {
            createParkingSpace.formError = true;
        } else {
            createParkingSpace.formError = false;
            const dailyRate = createParkingSpace.form.dailyRate;
            const nightlyRate = createParkingSpace.form.nightlyRate;
            const address = createParkingSpace.form.address;

            $uibModalInstance.close();

            // We define the parking space creation endpoint.
            const createParkingSpaceEndpoint =
                apiBaseURL +
                `create-parkingSpace?address=${address}&dailyRate=${dailyRate}&nightlyRate=${nightlyRate}`;

            // We hit the endpoint to create the parking space and handle success/failure responses.
            $http.get(createParkingSpaceEndpoint).then(
                (result) => createParkingSpace.displayMessage(result),
                (result) => createParkingSpace.displayMessage(result)
            );
        }
    };

    /** Displays the success/failure response from attempting to create an IOU. */
    createParkingSpace.displayMessage = (message) => {
        $rootScope.$broadcast("LoadingSpinner", false);
        const createParkingSpaceMsgModal  = $uibModal.open({
            templateUrl: 'createParkingSpaceMsgModal.html',
            controller: 'CreateParkingSpaceMsgModalCtrl',
            controllerAs: 'createParkingSpaceMsgModal',
            resolve: {
                message: () => message
            }
        });
        // No behaviour on close / dismiss.
        createParkingSpaceMsgModal .result.then(() => {}, () => {});
    };

    /** Closes the parking space creation modal. */
    createParkingSpace.cancel = () => $uibModalInstance.dismiss();

    // Validates the parking space data.
    function invalidFormInput() {
        return isNaN(createParkingSpace.form.dailyRate === undefined) && isNaN(createParkingSpace.form.nightly === undefined) && !createParkingSpace.form.address && createParkingSpace.form.address.trim().length === 0;
    }
});

// Controller for the success/fail modal.
angular.module('demoAppModule').controller('CreateParkingSpaceMsgModalCtrl', function($uibModalInstance, message) {
    const createParkingSpaceMsgModal = this;
    createParkingSpaceMsgModal.message = message.data;
});