"use strict";

// Define your backend here.
angular.module('demoAppModule', ['ui.bootstrap']).controller('DemoAppCtrl', function($rootScope, $http, $location, $uibModal) {
    const demoApp = this;
    demoApp.shouldLoad = false;
    const apiBaseURL = "/api/parking/";
    $rootScope.$on("LoadingSpinner", function (event, args) {
        demoApp.shouldLoad = args;
    });


    // Retrieves the identity of this and other nodes.
    let peers = [];
    $http.get(apiBaseURL + "me").then((response) => demoApp.thisNode = response.data.me);
    $http.get(apiBaseURL + "peers").then((response) => peers = response.data.peers);

    demoApp.availableSpaces = () => {
        $http.get(apiBaseURL + "available-spaces").then((response) =>
            demoApp.freeParkingSpaces = Object.keys(response.data).map((key) => response.data[key].state.data));
    };

    demoApp.allParkingSpaces = () => {

    },
    demoApp.availableSpaces();
    /** Displays the parking session starting modal. */
    demoApp.openStartParkingModal = () => {
        const startParkingModal = $uibModal.open({
            templateUrl: 'startParkingModal.html',
            controller: 'StartParkingModalCtrl',
            controllerAs: 'startParkingModal',
            resolve: {
                apiBaseURL: () => apiBaseURL,
                freeParkingSpaces: () => demoApp.freeParkingSpaces
            }
        });

        // Ignores the modal result events.
        startParkingModal.result.then(() => {demoApp.shouldLoad = true}, () => {});
    };

    demoApp.openCreateParkingSpaceModal = () => {
        const createParkingSpaceModal = $uibModal.open({
            templateUrl: 'createParkingSpaceModal.html',
            controller: 'CreateParkingSpaceModalCtrl',
            controllerAs: 'createParkingSpaceModal',
            resolve: {
                apiBaseURL: () => apiBaseURL
            }
        });

        // Ignores the modal result events.
        createParkingSpaceModal.result.then(() => {
            demoApp.shouldLoad = true}, () => {
        });
    };


    /** Displays the cash issuance modal. */
    demoApp.openIssueCashModal = () => {
        const issueCashModal = $uibModal.open({
            templateUrl: 'issueCashModal.html',
            controller: 'IssueCashModalCtrl',
            controllerAs: 'issueCashModal',
            resolve: {
                apiBaseURL: () => apiBaseURL
            }
        });

        issueCashModal.result.then(() => {demoApp.shouldLoad = true}, () => {});
    };


    demoApp.composeActiveParkings = () => {
        demoApp.activeTenantParkings = [];
        demoApp.activeRenterParkings = [];
        console.log("sessions :" + JSON.stringify(demoApp.activeSessions) + ", spaces: " + JSON.stringify(demoApp.allSpaces));
        for (let i=0; i < demoApp.activeSessions.length; i++) {
            for (let j=0; j < demoApp.allSpaces.length; j++) {
                console.log("session :" + JSON.stringify(demoApp.activeSessions[i]) + "and space : " + JSON.stringify(demoApp.allSpaces[j]));
                let session = demoApp.activeSessions[i];
                let space = demoApp.allSpaces[j];
                if (session.parkingSpaceReference.id === space.linearId.id) {
                   session.space = space;
                    console.log("YES");
                   if (session.tenant === demoApp.thisNode) {
                       console.log("TENANT");
                       demoApp.activeTenantParkings.push(session);
                   } else {
                       console.log("RENTER");
                       demoApp.activeRenterParkings.push(session);
                   }
                }
            }
        }
    };

    demoApp.getActiveSessionsData = () => {
        $http.get(apiBaseURL + "parkings").then((response) => {
            demoApp.activeSessions = Object.keys(response.data).map((key) => response.data[key].state.data);
            console.log("GOT sessions: " + JSON.stringify(demoApp.activeSessions));
            $http.get(apiBaseURL + "spaces").then((response) =>{

                demoApp.allSpaces = Object.keys(response.data).map((key) => response.data[key].state.data);
                console.log("GOT SPACES:" + JSON.stringify(demoApp.allSpaces));
                demoApp.composeActiveParkings();
            });
        });
    };

    /** Refreshes the front-end. */
    demoApp.refresh = () => {
        // Get available spaces.
        demoApp.availableSpaces();


        // Update the list of active parkings.
       demoApp.getActiveSessionsData();

        // Update the cash balances.
        $http.get(apiBaseURL + "cash-balances").then((response) => demoApp.cashBalances =
            response.data);


    };


    demoApp.finishParking = (parkingSessionId, parkingSpaceId) => {
        const finishParkingEndpoint =
            apiBaseURL +
            `finish-parkingSession?sessionId=${parkingSessionId}&spaceId=${parkingSpaceId}`;

        $http.get(finishParkingEndpoint).then(
            (result) => {
                demoApp.refresh()
            },
            (result) => {
                demoApp.refresh()
            });
    };

    demoApp.hasMinimumCashBalance = () => {
        if (demoApp.cashBalances === undefined) {
            return false;
        }
        let amountWithCurrency = demoApp.cashBalances.USD;
        if (amountWithCurrency === undefined) {
            return false;
        }
        let amount = Number(amountWithCurrency.split(" ")[0]);

        return amount > 50;
    };
    demoApp.refresh();
});

// Causes the webapp to ignore unhandled modal dismissals.
angular.module('demoAppModule').config(['$qProvider', function($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);