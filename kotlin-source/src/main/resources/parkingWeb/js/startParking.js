"use strict";

angular.module('demoAppModule').controller('StartParkingModalCtrl', function($rootScope, $http, $uibModalInstance, $uibModal, apiBaseURL, freeParkingSpaces) {
    const startParking = this;

    startParking.freeParkingSpaces = freeParkingSpaces;
    startParking.form = {};
    startParking.formError = false;

    startParking.create = () => {
        if (invalidFormInput()) {
            startParking.formError = true;
        } else {
            startParking.formError = false;
            const space = startParking.form.parkingSpace;
            const party = space.owner.split(",")[2].split("=")[1];
            const spaceId = space.linearId.id;

            $uibModalInstance.close();

            // We define the parking session start endpoint.
            const startParkingSessionEndpoint =
                apiBaseURL +
                `start-parkingSession?party=${party}&spaceId=${spaceId}`;

            // We hit the endpoint to start new parking session and handle success/failure responses.
            $http.get(startParkingSessionEndpoint).then(
                (result) => startParking.displayMessage(result),
                (result) => startParking.displayMessage(result)
            );
        }
    };

    /** Displays the success/failure response from attempting to create an IOU. */
    startParking.displayMessage = (message) => {
        $rootScope.$broadcast("LoadingSpinner", false);
        const startParkingMsgModal = $uibModal.open({
            templateUrl: 'startParkingMsgModal.html',
            controller: 'StartParkingMsgModalCtrl',
            controllerAs: 'startParkingMsgModal',
            resolve: {
                message: () => message
            }
        });
        // No behaviour on close / dismiss.
        startParkingMsgModal.result.then(() => {}, () => {});
    };

    startParking.cancel = () => $uibModalInstance.dismiss();

    function invalidFormInput() {
        return isNaN(startParking.form.counterparty === undefined);
    }
});

// Controller for the success/fail modal.
angular.module('demoAppModule').controller('StartParkingMsgModalCtrl', function($uibModalInstance, message) {
    const startParkingMsgModal = this;
    startParkingMsgModal.message = message.data;
});