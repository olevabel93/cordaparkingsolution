"use strict";

angular.module('demoAppModule').controller('MsgModalCtrl', function($uibModalInstance, result) {
    const settleMsgModal = this;
    settleMsgModal.result = result.data;
});