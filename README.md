

# The parking solution CorDapp

Parking solution created as a part of Master Thesis  **Case Study: Analysing parking solution using Corda DLT service**. This CorDapp intends to showcase the practical part of the CorDapp development. This document helps reader to set up, run and test the main fucntionalities of the CorDapp.

# Pre-Requisites

- **Oracle JDK 8 JVM** - minimum supported version **8u131**
- **IntelliJ IDEA** - supported versions **2017.1**, **2017.2** and **2017.3**
- **Git**

Also Gradle and Kotlin is used, but their are not needed to install. A standalone Gradle wrapper is provided, and it will download the correct version of Kotlin.

If you have any trouble setting up the CorDapp visit [Corda documentation site](https://docs.corda.net/getting-set-up.html)

# Instructions for setting up

1. `git clone https://olevabel93@bitbucket.org/olevabel93/cordaparkingsolution.git  `
2. `cd cordaparkingsolution`
3. `./gradlew deployNodes` - building may take upto a minute (it's much quicker if you already have the Corda binaries).
4. `cd kotlin-source/build/nodes`
5. `./runnodes`

At this point you will have notary/network map node running as well as three other nodes and their corresponding webservers. There should be 7 console windows in total. One for the networkmap/notary and two for each of the three nodes. The nodes take about 20-30 seconds to finish booting up.

# Using the CorDapp via the web front-end

In your favourite web browers, navigate to:

1. PartyA: `http://localhost:10007/web/parking`
2. PartyB: `http://localhost:10010/web/parking`
3. PartyC: `http://localhost:10013/web/parking`



Once booted up you should see something similar to this screenshot

![Screenshot](./docs/images/parking_application_home_screen.png)

You have four buttons on the navigation bar:

- Issue cash - will present model to issue cash. Needed in order to start parking
- Start parking - will present model to start new parkign session. Will be enabled once more than 50 USD are issued.
- Create space - will present model to create new parking space.
- Refresh - will refresh the home view as well as data models.

## Self issue some cash

From the tenant UI:

1. Click the issue cash button
2. Enter amount
3. Click "issue cash"
4. Wait for the transaction confirmation
5. Click anywhere to dismiss message about the transaction
6. Click the refresh button
7. You'll see the "Cash balances" section update

## Create parking space

1. Click on the "Crate space" button.
2. Set daily hourly rate, nightly hourly rate and Address of the parking space
3. Click "Create"
4. Wait for the transaction confirmation
5. Click anywhere to dismiss message about the transaction
6. Press the refresh button
7. Navigate to the counterparties dashboard. 
8. Press refresh button
9. Click "Start parking"
10. Click on available parking space selection
11. Verify that the previously created space address is on the list

## Start parking session

NOTE: **Currently both interval and maximum parking fee amount is hardcoded, but they can easily be made cofigurable in UI. To make testing easier the interval is set to 10 seconds and maximum parking fee to 50 EUR.**

From the tenant UI:

1. Click the "Start parking" button.
2. Select available parking space
3. Press the "Start" button
4. Wait for the confirmation
5. Click anywhere to dismiss message about the transaction
6. Press the refresh button
7. You will see the active parking session section updated.
8. Wait for 10 seconds and click refresh.
9. Observe the change in active parking session amount. The amount will increase by the amount of either daily or nightly rate depending on wheter the node time is between 6AM and 10PM or 10PM and 6AM. Please note the this is UTC time.
   1. Currently the maximum amount of parking fee is 50 EUR.
   2. Observe that if your parking session fee will exeed the maximum next interval the application will finish the parking automatically.
   3. Step 12.
10. Navigate to the Renter UI, click refresh, you'll see that parking session sections has been updated.
11. Finish session by clicking "Finish" on the active parking session section.
12. Click "Refresh" to see the change in cash balance and that there are no active parkings.




